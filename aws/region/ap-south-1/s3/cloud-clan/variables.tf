variable "aws_region" {
  type        = string
  description = "aws region for provisioning infrastructure"
  default     = "ap-south-1"
}

variable "extra_tags" {
  type        = map(any)
  description = "extra tags for resources"
}