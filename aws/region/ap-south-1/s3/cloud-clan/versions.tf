terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.61.0"
    }
  }
  required_version = ">= 1.0"
  backend "http" {}
}

provider "aws" {
  region = var.aws_region
}
