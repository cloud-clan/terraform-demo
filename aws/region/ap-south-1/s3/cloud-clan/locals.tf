locals {
  bucket_name = basename(abspath(path.module))
  tags = {
    ManagedBy = "terraform"
    Team      = "Devops"
  }
}
